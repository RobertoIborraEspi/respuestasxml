package Pregunta_2;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Marshaller;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Execution {
    public static void main(String[] args) {
        File origen = new File("Clientes.dat");
        File destino = new File("Clientes.xml");
        createFile(origen);
        transformaOosToXml(origen, destino);

    }
    public static void createFile(File file) {
        Client_List listaClientes = new Client_List();
        Cliente cliente1 = new Cliente(1,"Juan", "Ruiz Mateo", new ArrayList<>(Arrays.asList("654321987", "955632487")));
        Cliente cliente2 = new Cliente(2,"Ramón", "Pérez Santos", new ArrayList<>(Arrays.asList("641253987")));
        Cliente cliente3 = new Cliente(3,"Emilio", "Navarro Sanchez", new ArrayList<>(Arrays.asList("632589741", "966325847", "965874235")));
        List<Cliente> clientes = new ArrayList<>();
        clientes.add(cliente1);
        clientes.add(cliente2);
        clientes.add(cliente3);
        listaClientes.setClientes(clientes);
        FileOutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(file);
            oos = new ObjectOutputStream(fos);

            oos.writeObject(listaClientes);
            oos.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    public static void transformaOosToXml(File origen, File destino) {
        FileInputStream fis = null;
        ObjectInputStream ois = null;
        Client_List listaClientes = null;
        try {
            fis = new FileInputStream(origen);
            ois = new ObjectInputStream(fis);
            listaClientes = (Client_List) ois.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        try {
            JAXBContext context = JAXBContext.newInstance(Client_List.class);
            Marshaller m = context.createMarshaller();
            m.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, Boolean.TRUE);
            m.setProperty(Marshaller.JAXB_ENCODING, "UTF-8");
            try (FileWriter fw = new FileWriter(destino)) {
                m.marshal(listaClientes, fw);
            } catch (IOException ex) {
                System.err.println("Problemas durante la escritura del fichero..." + ex.getMessage());
            }
        } catch (JAXBException e) {
            e.printStackTrace();
        }
    }
}
