package Pregunta_2;

import javax.xml.bind.annotation.*;
import java.io.Serializable;
import java.util.ArrayList;

@XmlType(propOrder = {"id", "nombre", "apellidos", "telefonos"})
public class Cliente implements Serializable {
    int id;
    String nombre;
    String apellidos;
    ArrayList<String> telefonos;
    String email;
    float saldo;
    double auxiliar;

    public Cliente(int id, String nombre, String apellidos, ArrayList<String> telefonos) {
        this.id = id;
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.telefonos = telefonos;
    }


    @XmlAttribute
    public int getId() {
        return id;
    }
    public void setId(int id) {
        this.id = id;
    }
    @XmlElement(name = "nombre")
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    @XmlElement(name = "apellidos")
    public String getApellidos() {
        return apellidos;
    }
    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }
    @XmlElementWrapper(name = "telefonos")
    @XmlElement(name = "telefono")
    public ArrayList<String> getTelefonos() {
        return telefonos;
    }
    public void setTelefonos(ArrayList<String> telefonos) {
        this.telefonos = telefonos;
    }
    @XmlTransient
    public String getEmail() {
        return email;
    }
    public void setEmail(String email) {
        this.email = email;
    }
    @XmlTransient
    public float getSaldo() {
        return saldo;
    }
    public void setSaldo(float saldo) {
        this.saldo = saldo;
    }
    @XmlTransient
    public double getAuxiliar() {
        return auxiliar;
    }
    public void setAuxiliar(double auxiliar) {
        this.auxiliar = auxiliar;
    }
}
