package Pregunta_2;

import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import java.io.Serializable;
import java.util.List;

@XmlRootElement(name = "clientes")
public class Client_List implements Serializable {
    List<Cliente> clientes;

    public Client_List() {
    }
    @XmlElement(name = "cli")
    public List<Cliente> getClientes() {
        return clientes;
    }

    public void setClientes(List<Cliente> clientes) {
        this.clientes = clientes;
    }
}
