package Pregunta_5;

import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.Scanner;

public class ProvinciasXML {
    public static void main(String[] args) {
        List<Pueblo> puebloListList1 = new ArrayList<>();
        puebloListList1.add(new Pueblo(1, "Alicante", "331.577 hab", "3 m"));
        puebloListList1.add(new Pueblo(2, "Alcoy", "58.977 hab", "562 m"));
        puebloListList1.add(new Pueblo(3, "Ibi", "23.403 hab", "816 m"));
        puebloListList1.add(new Pueblo(4, "Jijona", "6879 hab", "453 m"));
        puebloListList1.add(new Pueblo(5, "Elche", "230.625 hab", "86 m"));
        List<Pueblo> puebloListList2 = new ArrayList<>();
        puebloListList2.add(new Pueblo(1, "Valencia", "800.215 hab", "16 m"));
        puebloListList2.add(new Pueblo(2, "Tavernes de la Valldigna", "17.201 hab", "15 m"));
        puebloListList2.add(new Pueblo(3, "Cullera", "21.918 hab", "2 m"));
        puebloListList2.add(new Pueblo(4, "Burjasot", "38.632 hab", "59 m"));
        List<Provincia> provinciaList = new ArrayList<>();
        provinciaList.add(new Provincia("Alicante", "5816 km²", "1.879.888 hab", puebloListList1));
        provinciaList.add(new Provincia("Valencia", "10.841,82 km²", "2.591.875 hab", puebloListList2));
        Provincias provincias = new Provincias(provinciaList);
        Document doc = creaDoc(provincias);
        creaXml(doc);

    }

    private static Document creaDoc(Provincias provincias) {
        Document doc = null;
        Scanner sc = new Scanner(System.in);
        boolean adicionalesPro = false, adicionalesPu = false, correcto, pregunta;
        String check;
        do {
            correcto = true;
            System.out.print("¿Añadir elementos adicionales a las provincias? [S/N] ");
            check = sc.nextLine().toLowerCase(Locale.ROOT);
            if (check.equals("s")) {
                adicionalesPro = true;
            }else if (check.equals("n")){
                adicionalesPro = false;
            }else {
                System.out.println("Escriba S o N por favor...");
                correcto = false;
            }
        }while (!correcto);
        do {
            correcto = true;
            System.out.print("¿Añadir elementos adicionales a los pueblos? [S/N] ");
            check = sc.nextLine().toLowerCase(Locale.ROOT);
            if (check.equals("s")) {
                adicionalesPu = true;
            }else if (check.equals("n")){
                adicionalesPu = false;
            }else {
                System.out.println("Escriba S o N por favor...");
                correcto = false;
            }
        }while (!correcto);
        try {
            DocumentBuilderFactory docFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = docFactory.newDocumentBuilder();
            doc = docBuilder.newDocument();
            Element rootElement = doc.createElement("provincias");
            doc.appendChild(rootElement);
            for (int i = 0; i < provincias.getProvinciaList().size(); i++) {
                Element provincia = doc.createElement("provincia");
                rootElement.appendChild(provincia);

                Element nombre = doc.createElement("nombre-pr");
                nombre.setTextContent(provincias.getProvinciaList().get(i).getNombrePro());
                provincia.appendChild(nombre);

                Element superficie = doc.createElement("superficie-pr");
                superficie.setTextContent(provincias.getProvinciaList().get(i).getSuperficiePro());
                provincia.appendChild(superficie);

                Element poblacion = doc.createElement("poblacion-pr");
                poblacion.setTextContent(provincias.getProvinciaList().get(i).getPoblacionPro());
                provincia.appendChild(poblacion);

                Element pueblos = doc.createElement("pueblos");
                for (int j = 0; j < provincias.getProvinciaList().get(i).getPueblos().size(); j++) {
                    Element pueblo = doc.createElement("pueblo");
                    pueblo.setAttribute("num", String.valueOf(provincias.getProvinciaList().get(i).getPueblos().get(j).getNum()));
                    pueblos.appendChild(pueblo);

                    Element nombrePu = doc.createElement("nombre-pu");
                    nombrePu.setTextContent(provincias.getProvinciaList().get(i).getPueblos().get(j).getNombrePu());
                    pueblo.appendChild(nombrePu);

                    Element poblacionPu = doc.createElement("poblacion-pu");
                    poblacionPu.setTextContent(provincias.getProvinciaList().get(i).getPueblos().get(j).getPoblacionPu());
                    pueblo.appendChild(poblacionPu);

                    Element altitud = doc.createElement("altitud");
                    altitud.setTextContent(provincias.getProvinciaList().get(i).getPueblos().get(j).getAltitud());
                    pueblo.appendChild(altitud);
                    do {
                        pregunta = true;
                        if (adicionalesPu) {
                            System.out.print("¿Añadir elementos adicionales al pueblo " +
                                    provincias.getProvinciaList().get(i).getPueblos().get(j).getNombrePu() + "? [S/N] ");
                            check = sc.nextLine().toLowerCase(Locale.ROOT);
                            if (check.equals("s")) {
                                boolean otro;
                                do {
                                    System.out.print("Introduce nombre de elemento adicional para " +
                                            provincias.getProvinciaList().get(i).getPueblos().get(j).getNombrePu() + ": ");
                                    String teclado = sc.nextLine();
                                    Element elemPu = doc.createElement(teclado);
                                    System.out.print("Introduce valor del elemento: ");
                                    teclado = sc.nextLine();
                                    elemPu.setTextContent(teclado);
                                    pueblo.appendChild(elemPu);
                                    do {
                                        otro = false;
                                        correcto = true;
                                        System.out.print("¿Desea añadir otro elemento? [S/N] ");
                                        check = sc.nextLine().toLowerCase(Locale.ROOT);
                                        if (check.equals("s")) {
                                            otro = true;
                                        }else if (check.equals("n")){
                                            otro = false;
                                        }else {
                                            System.out.println("Escriba S o N por favor...");
                                            correcto = false;
                                        }
                                    }while (!correcto);
                                } while (otro);
                            }else if (check.equals("n")){
                            }else {
                                System.out.println("Escriba S o N por favor...");
                                 pregunta = false;
                            }
                        }
                    }while (!pregunta);
                }
                provincia.appendChild(pueblos);
                do {
                    pregunta = true;
                    if (adicionalesPro) {System.out.print("¿Añadir elementos adicionales a la provincia " +
                            provincias.getProvinciaList().get(i).getNombrePro() + "? [S/N] ");
                        check = sc.nextLine().toLowerCase(Locale.ROOT);
                        if (check.equals("s")) {
                            boolean otro;
                            do {
                                otro = false;
                                System.out.print("Introduce nombre de elemento adicional para provincia " +
                                        provincias.getProvinciaList().get(i).getNombrePro() + ": ");
                                String teclado = sc.nextLine();
                                Element elemPro = doc.createElement(teclado);
                                System.out.print("Introduce valor del elemento: ");
                                teclado = sc.nextLine();
                                elemPro.setTextContent(teclado);
                                provincia.appendChild(elemPro);
                                do {
                                    correcto = true;
                                    System.out.print("¿Desea añadir otro elemento? [S/N] ");
                                    check = sc.nextLine().toLowerCase(Locale.ROOT);
                                    if (check.equals("s")) {
                                        otro = true;
                                    }else if (check.equals("n")){
                                        otro = false;
                                    }else {
                                        System.out.println("Escriba S o N por favor...");
                                        correcto = false;
                                    }
                                }while (!correcto);

                            } while (otro);
                        }else if (check.equals("n")){
                        }else {
                            System.out.println("Escriba S o N por favor...");
                            pregunta = false;
                        }
                    }
                }while (!pregunta);
            }
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }
        return doc;
    }

    private static void creaXml(Document doc) {
        try {
            FileWriter fw = new FileWriter("provincias.xml");
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty(OutputKeys.INDENT, "yes");

            DOMSource source = new DOMSource(doc);
            StreamResult result = new StreamResult(fw);

            transformer.transform(source, result);
        }catch (Exception e) {
            e.printStackTrace();
        }



    }
}
