package Pregunta_5;

import java.util.List;

public class Provincias {
    private List<Provincia> provinciaList;

    public Provincias(List<Provincia> provinciaList) {
        this.provinciaList = provinciaList;
    }

    public List<Provincia> getProvinciaList() {
        return provinciaList;
    }

    public void setProvinciaList(List<Provincia> provinciaList) {
        this.provinciaList = provinciaList;
    }
}
