package Pregunta_5;

import java.util.List;

public class Provincia {
   private String nombrePro;
   private String superficiePro;
   private String poblacionPro;
   private List<Pueblo> pueblos;

    public Provincia(String nombrePro, String superficiePro, String poblacionPro, List<Pueblo> pueblos) {
        this.nombrePro = nombrePro;
        this.superficiePro = superficiePro;
        this.poblacionPro = poblacionPro;
        this.pueblos = pueblos;
    }

    public String getNombrePro() {
        return nombrePro;
    }

    public void setNombrePro(String nombrePro) {
        this.nombrePro = nombrePro;
    }

    public String getSuperficiePro() {
        return superficiePro;
    }

    public void setSuperficiePro(String superficiePro) {
        this.superficiePro = superficiePro;
    }

    public String getPoblacionPro() {
        return poblacionPro;
    }

    public void setPoblacionPro(String poblacionPro) {
        this.poblacionPro = poblacionPro;
    }

    public List<Pueblo> getPueblos() {
        return pueblos;
    }

    public void setPueblos(List<Pueblo> pueblos) {
        this.pueblos = pueblos;
    }
}
