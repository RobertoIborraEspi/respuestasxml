package Pregunta_5;

public class Pueblo {
    private int num;
    private String nombrePu;
    private String poblacionPu;
    private String altitud;

    public Pueblo(int num, String nombrePu, String poblacionPu, String altitud) {
        this.num = num;
        this.nombrePu = nombrePu;
        this.poblacionPu = poblacionPu;
        this.altitud = altitud;
    }

    public int getNum() {
        return num;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public String getNombrePu() {
        return nombrePu;
    }

    public void setNombrePu(String nombrePu) {
        this.nombrePu = nombrePu;
    }

    public String getPoblacionPu() {
        return poblacionPu;
    }

    public void setPoblacionPu(String poblacionPu) {
        this.poblacionPu = poblacionPu;
    }

    public String getAltitud() {
        return altitud;
    }

    public void setAltitud(String altitud) {
        this.altitud = altitud;
    }
}
